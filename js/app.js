// Custom Functions
// ========================================

/**
 * Show "User not found" with the search doesn't match with any record in the database
 */
function userNotFound() {
    document.getElementById('search_result').innerHTML = '<p>User not found</p>';
}

/**
 * Clean result content - used when search input becomes empty again
 */
function resetList() {
    document.getElementById('search_result').innerHTML = '';
}

/**
 * Replace current content in search result with loading text - used while typing or searching
 */
function showLoading() {
    document.getElementById('search_result').innerHTML = '<p>loading...</p>';
}

/**
 * Performs a search to the API using the search terms and dupes status
 */
function runSearch() {

    showLoading();

    let url   = API_ENDPOINT + '/search_user';
    let terms = document.getElementById('search_terms').value;
    let dupes = document.getElementById('search_dupes').checked;

    if (terms.trim() == "") {
        resetList();
        return;
    }

    // This is how I would have done it in AJAX using jQuery
    // -----------------------------------------------------
    /*
    $.ajax({
        method: "POST",
        type: "JSON",
        url: url,
        data: { terms: terms, dupes: dupes }
    }).done(function(data) {
        if (data.length > 0) {
            printUserList(data);
        } else {
            userNotFound();
        }
    });
    */

    // This is how I've done the AJAX POST using fetch with Vanilla JS
    // ---------------------------------------------------------------
    let data = new FormData();
    data.append('terms', terms);
    data.append('dupes', dupes);

    let fetch_params = {
        method: "POST",
        body: data
    };

    fetch(url, fetch_params)
        .then(function(res){ return res.json(); })
        .then(function(data){
            if (data.length > 0) {
                printUserList(data);
            } else {
                userNotFound();
            }
        });
}

/**
 *
 * @param array user_list
 */
function printUserList(user_list) {

    // Print only first MAX_RESULT number of users
    let users = user_list.length > MAX_RESULTS ? user_list.slice(0, MAX_RESULTS) : user_list;

    // Print result header
    let message = user_list.length + " user" + (user_list.length > 1 ? "s" : "") + " found";
    if (user_list.length > users.length) {
        message = "Showing first " + users.length + " users (" + message + ")";
    }
    document.getElementById('search_result').innerHTML = '<p>' + message + '</p>';

    // Print user list - Loop the list and create li elements with user names
    let ul = createNode('ul');
    ul.id = 'user_list';

    users.map(function(user) {
        let li = createNode('li');
        li.innerHTML = user.first_name + " " + user.last_name;
        append(ul, li);
    });

    append(document.getElementById('search_result'), ul)
}

// Listen for events
// =================================================

let runSearchTimeout;

// Detect Keyup on search_terms input
document.getElementById("search_terms")
    .addEventListener("keyup", function(event) {
        // For a fast typing users, we'll start searching only 200ms after the user stops typing
        // This way we avoid creating unnecessary api requests
        showLoading();
        clearTimeout(runSearchTimeout);
        runSearchTimeout = setTimeout(runSearch, 200);
    });

// Detect changes on allow_duplicates checkbox
document.getElementById("search_dupes")
    .addEventListener("change", function(event) {
        runSearch();
    });
